#include "fieldcontrol.h"

#include <QNdefNfcTextRecord>
#include <QNdefNfcUriRecord>
#include <QUrl>
#include <QProcess>
#include <QDebug>

FieldControl::FieldControl(QObject *parent) : QObject(parent)
{
    // change m_action to NdefAction::WriteNdef or NdefAction::ReadNdef for manual testing
    m_action = NdefAction::ReadNdef;
    m_manager = new QNearFieldManager(this);
    connect(m_manager, &QNearFieldManager::targetDetected, this, &FieldControl::onTargetDetected);
    connect(m_manager, &QNearFieldManager::targetLost, this, &FieldControl::onTargetLost);
    qDebug() << "Start target detection.";
    // to stop target detecting use QNearFieldManager::stopTargetDetection()
}

void FieldControl::onTargetDetected(QNearFieldTarget *target)
{
    qDebug() << "Target detected.";
    switch (m_action) {
    case ReadNdef:
        qDebug() << "Read ndef case.";
        m_manager->setTargetAccessModes(QNearFieldManager::NdefReadTargetAccess);
        m_manager->startTargetDetection();

        connect(target, &QNearFieldTarget::ndefMessageRead, this, &FieldControl::onNdefMessageRead);
        connect(target, &QNearFieldTarget::error, this, &FieldControl::onTargetError);

        m_request = target->readNdefMessages();
        if(!m_request.isValid())
            onTargetError(QNearFieldTarget::NdefReadError, m_request);
        break;
    case WriteNdef:
        qDebug() << "Write ndef case.";
        m_manager->setTargetAccessModes(QNearFieldManager::NdefWriteTargetAccess);
        m_manager->startTargetDetection();

        connect(target, &QNearFieldTarget::ndefMessagesWritten, this, &FieldControl::onNdefMessageWritten);
        connect(target, &QNearFieldTarget::error, this, &FieldControl::onTargetError);

        m_request = target->writeNdefMessages(QList<QNdefMessage>() << ndefMessage());
        // QNearFieldTarget::writeNdefMessages is not working since Qt 5.5, use ndef-write instead if you want to write NTAG by application
        QProcess::execute("ndef-write -u \"https://www.google.com/\"");
        if(!m_request.isValid())
            onTargetError(QNearFieldTarget::NdefWriteError, m_request);
        break;
    default:
        break;
    }
}

void FieldControl::onTargetLost(QNearFieldTarget *target)
{
    qDebug() << "Target lost.";
    target->deleteLater();
}

void FieldControl::onNdefMessageRead(const QNdefMessage &message)
{
    qDebug() << "Message read.";
    for(const QNdefRecord &record : message){
        if(record.isRecordType<QNdefNfcTextRecord>()){
            QNdefNfcTextRecord textRecord = record;
            qDebug() << "Text record:" << textRecord.text();
        }else if(record.isRecordType<QNdefNfcUriRecord>()){
            QNdefNfcUriRecord uriRecord = record;
            qDebug() << "Uri record:" << uriRecord.uri().toString();
        }else if(record.typeNameFormat() == QNdefRecord::Mime && record.type().startsWith("image/")){
            // ToDo: add image format processing
            qDebug() << "Image record.";
        }else{
            qDebug() << "Unknown record.";
        }
    }
}

void FieldControl::onTargetError(QNearFieldTarget::Error error, const QNearFieldTarget::RequestId &request)
{
    if(m_request == request){
        switch (error) {
        case QNearFieldTarget::NoError:
            qDebug() << "No error.";
            break;
        case QNearFieldTarget::UnsupportedError:
            qDebug() << "Unsupported error.";
            break;
        case QNearFieldTarget::TargetOutOfRangeError:
            qDebug() << "Target out of range error.";
            break;
        case QNearFieldTarget::NoResponseError:
            qDebug() << "No response error.";
            break;
        case QNearFieldTarget::ChecksumMismatchError:
            qDebug() << "Checksum mismatch error.";
            break;
        case QNearFieldTarget::InvalidParametersError:
            qDebug() << "Invalid parameters error.";
            break;
        case QNearFieldTarget::NdefReadError:
            qDebug() << "Ndef read error.";
            break;
        case QNearFieldTarget::NdefWriteError:
            qDebug() << "Ndef write error.";
            break;
        default:
            qDebug() << "Default error.";
            break;
        }

        m_request = QNearFieldTarget::RequestId();
    }
}

void FieldControl::onNdefMessageWritten()
{
    qDebug() << "Message written.";
}

QNdefMessage FieldControl::ndefMessage() const
{
    // ToDo: switch QNdefNfcTextRecord or QNdefNfcUriRecord
    qDebug() << "Writing message.";
    QNdefMessage message;
    QNdefNfcUriRecord record;
    record.setUri(QUrl("https://www.google.com/"));
    message.append(record);
    return message;
}
