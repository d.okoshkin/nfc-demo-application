#include <sailfishapp.h>

#include <QGuiApplication>

#include "fieldcontrol.h"

int main(int argc, char *argv[])
{
    FieldControl nfc;

    QGuiApplication *app = SailfishApp::application(argc, argv);

    return app->exec();
}
