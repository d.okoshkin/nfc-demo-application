#ifndef FIELDCONTROL_H
#define FIELDCONTROL_H

#include <QObject>
#include <QNearFieldManager>
#include <QNearFieldTarget>
#include <QNdefMessage>
#include <QNdefRecord>

class FieldControl : public QObject
{
    Q_OBJECT
public:
    explicit FieldControl(QObject *parent = nullptr);

public slots:
    void onTargetDetected(QNearFieldTarget *target);
    void onTargetLost(QNearFieldTarget *target);
    void onNdefMessageRead(const QNdefMessage &message);
    void onTargetError(QNearFieldTarget::Error error, const QNearFieldTarget::RequestId &request);
    void onNdefMessageWritten();

private:
    enum NdefAction {
        ReadNdef,
        WriteNdef
    };
    NdefAction m_action;
    QNdefMessage ndefMessage() const;

    QNearFieldManager *m_manager;
    QNearFieldTarget::RequestId m_request;
};

#endif // FIELDCONTROL_H
