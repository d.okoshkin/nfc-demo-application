TARGET = nfc-demo-application

CONFIG += sailfishapp

QT += nfc

SOURCES += src/nfc-demo-application.cpp \
    src/fieldcontrol.cpp

DISTFILES += \
    rpm/nfc-demo-application.spec \
    rpm/nfc-demo-application.yaml

CONFIG += sailfishapp_i18n

HEADERS += \
    src/fieldcontrol.h
